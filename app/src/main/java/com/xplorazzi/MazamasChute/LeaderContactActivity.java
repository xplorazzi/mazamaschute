package com.xplorazzi.MazamasChute;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LeaderContactActivity extends AppCompatActivity implements
        SearchResultsAdapter.OnPhoneCallListener,
        SearchResultsAdapter.OnWriteEmailListener,
        SearchResultsAdapter.OnMapAddressListener {

    private static String TAG = "LeaderContactActivity";

    private Typeface type;
    private ListView searchResults;
    private String found = "N";

    //This arraylist will have data as pulled from server. This will keep cumulating.
    private static ArrayList<Contact> productResults = new ArrayList<Contact>();
    //Based on the search string, only filtered products will be moved here from productResults
    //private static ArrayList<Contact> filteredProductResults = new ArrayList<Contact>();

    public interface OnLeaderSearchListener {
        boolean onSearch();
    }

    private OnLeaderSearchListener mLeaderSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.d(TAG, "onCreate");

        if (((MainActivity) (MainActivity.activity)).onSearch() == true) {
            searchResults = (ListView) findViewById(R.id.listview_search);
            searchResults.setVisibility(searchResults.VISIBLE);
            Log.d(TAG, "Getting Leader Contacts");
            myAsyncTask m = (myAsyncTask) new myAsyncTask().execute("##NOFILTER##");
        }
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }

    /*
    public void filterProductArray(String newText) {
        String pFirstName, pLastName;

        filteredProductResults.clear();

        for (int i = 0; i < productResults.size(); i++) {
            pFirstName = productResults.get(i).getFirstName().toLowerCase();
            pLastName = productResults.get(i).getLastName().toLowerCase();
            if (newText.equals("##NOFILTER##")) {
                filteredProductResults.add(productResults.get(i));
            } else if (pFirstName.contains(newText.toLowerCase()) ||
                    pLastName.contains(newText.toLowerCase())) {
                filteredProductResults.add(productResults.get(i));
            }
        }
    }
   */

    class myAsyncTask extends AsyncTask<String, Void, String> {
        List<Contact> productList;
        String textSearch;
        ProgressDialog pd;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            productList = new ArrayList<Contact>();

            //pd= new ProgressDialog(getActivity());
            pd = new ProgressDialog(LeaderContactActivity.this);
            pd.setCancelable(false);
            pd.setMessage("Searching Leaders...");
            pd.getWindow().setGravity(Gravity.CENTER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... sText) {
            Log.d(TAG, "doInBackground");
            String returnResult = getAllLeaderContactList();
            this.textSearch = sText[0];
            return returnResult;
        }

        public String getAllLeaderContactList() {
            Contact tempProduct;
            String matchFound = "N";
            //productResults is an arraylist with all product details for the search criteria
            //productResults.clear();
            Log.d(TAG, "getAllLeaderContactList");
            try {
                productList = DataBaseHelper.getAllLeaderContacts();
                //productList = DataBaseHelper.getAllMemberContacts();

                //parse date for dateList
                for (int i = 0; i < productList.size(); i++) {
                    tempProduct = new Contact();
                    Contact obj = productList.get(i);

                    tempProduct.setFirstName(obj.getFirstName());
                    tempProduct.setLastName(obj.getLastName());
                    tempProduct.setMidName(obj.getMidName());
                    tempProduct.setAddress(obj.getAddress());
                    tempProduct.setCity(obj.getCity());
                    tempProduct.setState(obj.getState());
                    tempProduct.setZIP(obj.getZIP());
                    tempProduct.setPhoneNumber(obj.getPhoneNumber());
                    tempProduct.setEmail(obj.getEmail());

                    //check if this product is already there in productResults, if yes, then don't add it again.
                    matchFound = "N";

                    for (int j = 0; j < productResults.size(); j++) {
                        if (productResults.get(j).getFirstName().equals(tempProduct.getFirstName()) &&
                                productResults.get(j).getLastName().equals(tempProduct.getLastName()) &&
                                productResults.get(j).getMidName().equals(tempProduct.getMidName())) {
                            matchFound = "Y";
                        }
                    }
                    if (! matchFound.equals("N")) {
                        productResults.add(tempProduct);
                    }

                }
                return ("OK");
            } catch (Exception e) {
                e.printStackTrace();
                return ("Exception Caught");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase("Exception Caught")) {
                Toast.makeText(LeaderContactActivity.this, "Unable to connect to server,please try later", Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                //calling this method to filter the search results from productResults and move them to
                //filteredProductResults
                //filterProductArray(textSearch);
                searchResults.setAdapter(new SearchResultsAdapter(LeaderContactActivity.this, LeaderContactActivity.this, productResults));
                pd.dismiss();
            }
        }
    }

    public void onPhoneCall(String text) {
        Log.d(TAG, "Making Phone call");
        String number = "tel:" + text.trim();
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    public void onWriteEmail(String text) {
        Log.d(TAG, "Writing Email");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{text});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT, "body of email");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(LeaderContactActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onMapAddress (String text) {
        Log.d(TAG, "Mapping Address");
        //text = text.replace(',','+');
        //text = text.replace(' ','+');
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=0,0 (%s)", text);
        //String uri = String.format(Locale.ENGLISH, "geo:0,0?q=", text);
        Log.d(TAG, "URI:" + uri);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            //    Log.e(TAG, ex.toString());

            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(LeaderContactActivity.this, "Please install a maps application", Toast.LENGTH_LONG).show();
            }

        }
    }
}
