package com.xplorazzi.MazamasChute;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Pranabesh on 11/27/2015.
 */
public class SearchResultsAdapter extends BaseAdapter {
    private static String TAG = "SearchAdapter";
    private LayoutInflater layoutInflater;
    //private SearchMemberFragment mSearchMemberFragment;
    private Object mImplObj;

    private ArrayList<Contact> productDetails = new ArrayList<Contact>();
    int count;
    Typeface type;
    Context context;

    //constructor method
    public SearchResultsAdapter(Context context, Object intImpl, ArrayList<Contact> product_details) {

        layoutInflater = LayoutInflater.from(context);

        this.productDetails = product_details;
        this.count = product_details.size();
        this.context = context;
        this.mImplObj = intImpl;
        type = Typeface.createFromAsset(context.getAssets(), "fonts/book.TTF");
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int arg0) {
        return productDetails.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listto_search, null);
            holder = new ViewHolder();
            holder.first_name = (TextView) convertView.findViewById(R.id.first_name);
            holder.last_name = (TextView) convertView.findViewById(R.id.last_name);
            holder.mid_name = (TextView) convertView.findViewById(R.id.mid_name);
            holder.address = (Button) convertView.findViewById(R.id.mapAddress);
            holder.address.setOnClickListener(openMap);
            holder.phone = (Button) convertView.findViewById(R.id.phoneCall);
            holder.phone.setOnClickListener(makeCall);
            holder.email = (Button) convertView.findViewById(R.id.writeEmail);
            holder.email.setOnClickListener(openMail);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (productDetails.size() == 0) return convertView;

        Contact tempProduct = productDetails.get(position);

        holder.first_name.setText(tempProduct.getFirstName());
        holder.first_name.setTypeface(type);

        holder.last_name.setText(tempProduct.getLastName());
        holder.last_name.setTypeface(type);

        holder.mid_name.setText(tempProduct.getMidName());
        holder.mid_name.setTypeface(type);

        holder.address.setText(tempProduct.getAddress() + ", " + tempProduct.getCity() + ", " + tempProduct.getState() + " " + tempProduct.getZIP());
        holder.address.setTypeface(type);

        holder.phone.setText(tempProduct.getPhoneNumber());
        holder.email.setText(tempProduct.getEmail());

        return convertView;
    }

    static class ViewHolder {
        TextView first_name;
        TextView last_name;
        TextView mid_name;
        Button address;
        Button email;
        Button phone;
    }

    View.OnClickListener makeCall = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "makeCall clicked");
            Button b = (Button) v;
            String text = b.getText().toString();
            ((OnPhoneCallListener) mImplObj).onPhoneCall(text);
        }
    };

    View.OnClickListener openMail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button b = (Button) v;
            String text = b.getText().toString();
            ((OnWriteEmailListener) mImplObj).onWriteEmail(text);
        }
    };

    View.OnClickListener openMap = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button b = (Button) v;
            String text = b.getText().toString();
            ((OnMapAddressListener) mImplObj).onMapAddress(text);
        }
    };

    public interface OnPhoneCallListener {
        void onPhoneCall(String text);
    }

    public interface OnWriteEmailListener {
        void onWriteEmail(String text);
    }

    public interface OnMapAddressListener {
        void onMapAddress(String text);
    }
}

