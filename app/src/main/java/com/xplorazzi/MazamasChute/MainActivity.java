package com.xplorazzi.MazamasChute;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;


public class MainActivity extends AppCompatActivity implements
        FragmentSimpleLoginButton.OnCompleteListener,
        SearchMemberFragment.OnSearchListener,
        LeaderContactActivity.OnLeaderSearchListener {

    public static final String FB_FRAGMENT_TAG = "fb_fragment_tag";
    public static final String SEARCH_FRAGMENT_TAG = "search_fragment_tag";
    private static final String TAG = "MainActivity";
    private FragmentManager mFragmentManager;
    private TextView isMember;
    private FragmentSimpleLoginButton fragmentSimpleLoginButton;
    private SearchMemberFragment searchMember;

    private DataBaseHelper mDbHelper;

    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;

        Log.d(TAG, "MainActivity:onCreate");
        mFragmentManager = getSupportFragmentManager();
        isMember = (TextView) findViewById(R.id.isMember);
        Fragment fb_fragment = mFragmentManager.findFragmentByTag(FB_FRAGMENT_TAG);
        Fragment search_fragment = mFragmentManager.findFragmentByTag(SEARCH_FRAGMENT_TAG);

        FragmentTransaction transaction = mFragmentManager.beginTransaction();

        Log.d(TAG, "New SearchMemberFragment");
        searchMember = new SearchMemberFragment();
        transaction.add(android.R.id.content, searchMember, SEARCH_FRAGMENT_TAG);

        fragmentSimpleLoginButton = new FragmentSimpleLoginButton();
        Log.d(TAG, "New fragmentSimpleLoginButton created");
        transaction.add(android.R.id.content, fragmentSimpleLoginButton, FB_FRAGMENT_TAG);
        Log.d(TAG, "Debug stack:" + mFragmentManager.getBackStackEntryCount());
        transaction.commit();

        mDbHelper = new DataBaseHelper(this);
        mDbHelper.readFromDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_leader_profiles:
                showToast("Leader Profiles Clicked");
                intent = new Intent(this, LeaderContactActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.action_map_members:
                showToast("Map Members Clicked");
                intent = new Intent(this, MapAllActivity.class);
                this.startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "Fragment Backstack count: " + mFragmentManager.getBackStackEntryCount());
        if (mFragmentManager.getBackStackEntryCount() != 0) {
            mFragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MainActivity:onResume");
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity:onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MainActivity:onPause");

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity:onDestroy");
        loginCheckMessage();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "Save State");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "Restore State");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragmentSimpleLoginButton.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
    }

    private boolean loginCheckMessage() {
        if (fragmentSimpleLoginButton != null) {
            if (fragmentSimpleLoginButton.profileIsLoggedIn() && fragmentSimpleLoginButton.profileIsAMember()) {
                isMember.setText("You are a Member");
                searchMember.setQueryHint("Member Search...");
                return true;
            } else if (fragmentSimpleLoginButton.profileIsLoggedIn() && fragmentSimpleLoginButton.profileIsAMember() == false) {
                isMember.setText("You are not a Member");
                searchMember.setQueryHint("Members Directory...");
                searchMember.clearSearchLists();
                return false;
            } else {
                isMember.setText("Login to check Mazama Membership");
                searchMember.setQueryHint("Members Directory...");
                searchMember.clearSearchLists();
                return false;
            }
        } else {
            Log.d(TAG, "fragmentSimpleLoginButton is null");
            return false;
        }
    }

    public void onComplete() {
        Log.d(TAG, "onComplete");
        // After the fragment completes, it calls this callback.
        // setup the rest of your layout now
        loginCheckMessage();
    }

    public boolean onSearch() {
        Log.d(TAG, "onSearch");
        return loginCheckMessage();
    }
 }
