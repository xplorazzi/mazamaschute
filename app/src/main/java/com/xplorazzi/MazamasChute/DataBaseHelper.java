package com.xplorazzi.MazamasChute;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pranabesh on 11/9/2015.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    //The	Android's	default	system	path	of	your	application	database.
    private static String TAG = "DataBaseHelper"; // Tag just for the LogCat window
    private static String DB_PATH = ""; //"/data/data/com.xplorazzi.loginsample/databases/";
    private static String DB_NAME = "MazamaMembers.db";
    private SQLiteDatabase mDataBase;
    private final Context mContext;

    private static boolean readDB = true;
    private static SQLiteDatabase mDb;

    private static List<Contact> memberContactList;
    private static List<Contact> leaderContactList;

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Contacts table name
    private static final String TABLE_MEMBER_CONTACTS = "MazamaMembers";
    private static final String TABLE_LEADER_CONTACTS = "MazamaLeaders";

    // Contacts Table Columns names
    private static final String KEY_ID = "_id";
    private static final String KEY_FIRST_NAME = "FirstName";
    private static final String KEY_MIDDLE_NAME = "MiddleName";
    private static final String KEY_LAST_NAME = "LastName";
    private static final String KEY_ADDRESS = "Address";
    private static final String KEY_STATE = "State";
    private static final String KEY_ZIP = "ZIP";
    private static final String KEY_PH_NO = "phone_number";
    private static final String KEY_EMAIL = "Email";

    private static String[] columnNames =
            {"Last Name", "First Name", "Middle Name",
                    "Street", "City", "State", "ZIP",
                    "Email", "Phone"};
    public static String [][] data = {
            {"Aanerud",	"Sherry",	"",	"7633 SW 51st Pl",	"Portland",	"OR",	"97219",	"vanaan@yahoo.com",	"503-293-2894"},
            {"Aaroe",	"David",	"W",	"236 S.W. Parkside Dr.",	"Portland",	"OR",	"97205",	"davida@fortisconstruct.com",	"971-563-1929"},
            {"Aarsund",	"Stan",	"",	"10117 SE Sunnyside Rd suite F #1171",	"Clackamas",	"OR",	"97015",	"hikerstan@hotmail.com",	"971-645-9943"},
            {"Aarts",	"Wim",	"",	"11533 SW 55th Ave",	"Portland",	"OR",	"97219",	"waarts@gmail.com",	"503-297-8774"},
            {"Abbott",	"James",	"D",	"1960 SW Taylors Ferry Rd",	"Portland",	"OR",	"97219",	"ukulelejim@comcast.net",	"503-293-0436"},
            {"Dash",	"Pranabesh",	"E",	"16786 Lake Forest Blvd",	"Lake Oswego",	"OR",	"97035",	"pranabesh.dash@gmail.com",	"503-593-4942"}
    };

    //private final String assetPath;
    //private final String dbPath;

    /**
     * Constructor
     *  	*	Takes	and	keeps	a	reference	of	the	passed	context	in	order	to	access	to	the	application	assets	and	resources.
     *  	*	@param	context
     *  
     */
    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);

        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        /*
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else         {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        */
        this.mContext = context;
        checkExists();
    }

    /**
     *  	*	Creates	a	empty	database	on	the	system	and	rewrites	it	with	your	own	database.
     *  	*
     */
    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            //do	nothing	-	database	already	exist
        } else {
            //By	calling	this	method	and	empty	database	will	be	created	into	the	default	system	path
            //of	your	application	so	we	are	gonna	be	able	to	overwrite	that	database	with	our	database.
            this.getReadableDatabase();
            this.close();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("CreateDatabase: Error	copying	database " + DB_PATH);
            }
        }
    }

    private void checkExists() {
        Log.i(TAG, "checkExists()");
        File dbFile = new File(DB_PATH + DB_NAME);
        if (!dbFile.exists()) {
            Log.i(TAG, "creating database..");
            dbFile.getParentFile().mkdirs();
            try {
                InputStream myInput = mContext.getAssets().open("databases/"+ DB_NAME);
                /*
                Log.d(TAG, "File path " + DB_PATH );
                File file = new File ("/data/data/com.xplorazzi.loginsample/databases");
                if (file.exists()) {
                    Log.d(TAG, "/Data/data/com.xpz/db File Exists");
                } else {
                    Log.d(TAG, "/Data/data/com.xpz/db File not found");
                }
                if (myInput == null) {
                    Log.d(TAG, "Input stream is null");
                }
                */
                copyStream(myInput, new FileOutputStream(
                        dbFile));
            } catch (IOException e) {
                throw new Error("CheckExists: Error	copying	database " + mContext.getAssets().getLocales().toString());
            }
            Log.i(TAG, DB_NAME + " has been copied to " + dbFile.getAbsolutePath());
        }
    }

    private void copyStream(InputStream is, OutputStream os) throws IOException {
        byte buf[] = new byte[1024];
        int c = 0;
        while (true) {
            c = is.read(buf);
            if (c == -1)
                break;
            os.write(buf, 0, c);
        }
        is.close();
        os.close();
    }
    /**
     *  	*	Check	if	the	database	already	exist	to	avoid	re-copying	the	file	each	time	you	open	the	application.
     *  	*	@return	TRUE	if	it	exists,	FALSE	if	it	doesn't
     *  
     */
    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        //try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        //} catch (SQLiteException e) {
        //    throw new Error("checkDatabase: Unable to find Database " + DB_PATH + DB_NAME);
            //database	does't	exist	yet.
        //}
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    /**
     *  	*	Copies	your	database	from	your	local	assets-folder	to	the	just	created	empty	database	in	the
     *  	*	system	folder,	from	where	it	can	be	accessed	and	handled.
     *  	*	This	is	done	by	transfering	bytestream.
     *  	*
     */

    private void copyDataBase() throws IOException {
        /*
         * Close SQLiteOpenHelper so it will commit the created empty database
         * to internal storage.
         */
        close();

        /*
         * Open the database in the assets folder as the input stream.
         */
        InputStream myInput = mContext.getAssets().open(DB_NAME);

        /*
         * Open the empty db in interal storage as the output stream.
         */
        OutputStream myOutput = new FileOutputStream(DB_PATH);

        /*
         * Copy over the empty db in internal storage with the database in the
         * assets folder.
         */

        copyFile (myInput, myOutput);

        /*
         * Access the copied database so SQLiteHelper will cache it and mark it
         * as created.
         */
        getWritableDatabase().close();
    }

    public void copyFile(InputStream is, OutputStream op) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(is);
        BufferedOutputStream bos = new BufferedOutputStream(op);
        byte[] bt = new byte[8192];
        int len = bis.read(bt);
        while (len != -1) {
            bos.write(bt, 0, len);
            len = bis.read(bt);
        }
        bis.close();
        bos.close();
    }

    /*
    private void copyDataBase() throws IOException {
        //Open	your	local	db	as	the	input	stream
        InputStream myInput = mContext.getAssets().open(DB_NAME);

        //	Path	to	the	just	created	empty	db
        String outFileName = DB_PATH + DB_NAME;

        //Open	the	empty	db	as	the	output	stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer	bytes	from	the	inputfile	to	the	outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close	the	streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }
    */

    public void openDataBase() throws SQLException {

        //Open	the	database
        String myPath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    //	Add	your	public	helper	methods	to	access	and	get	content	from	the	database.
    //	You	could	return	cursors	by	doing	return myDataBase.query(....)	so	it'd	be	easy
    //	to	you	to	create	adapters	for	your	views.

    public void readFromDatabase() {

        if (!readDB) return;

        try {
            createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create Database");
        }
        try {
            openDataBase();
            close();
            mDb = getReadableDatabase();
        } catch (SQLException sqle) {
            Log.e(TAG, "open >>"+ sqle.toString());
            throw sqle;
        }
    }

    public static List<Contact> getAllMemberContacts() {

        if (memberContactList == null) {
            memberContactList = new ArrayList<Contact>();
        }

        if (memberContactList.size() != 0) {
            Log.d(TAG, "List has " + memberContactList.size() + " elements.");
            return memberContactList;
        }
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MEMBER_CONTACTS;

        Log.d (TAG, "getAllMemberContacts");

        if (!readDB) {
            int rowCnt = data.length;
            int colCnt = data[0].length;
            for (int row = 0; row < rowCnt; row++) {
                for (int col = 0; col < colCnt; col++) {
                    Contact contact = new Contact();
                    contact.setLastName(data[row][0]);
                    contact.setFirstName(data[row][1]);
                    contact.setMidName(data[row][2]);
                    contact.setAddress(data[row][3]);
                    contact.setCity(data[row][4]);
                    contact.setState(data[row][5]);
                    contact.setZIP(data[row][6]);
                    contact.setEmail(data[row][7]);
                    contact.setPhoneNumber(data[row][8]);
                    // Adding contact to list
                    memberContactList.add(contact);
                }
            }
            return memberContactList;
        }

        //SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = mDb.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                // contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setLastName(cursor.getString(1));
                contact.setFirstName(cursor.getString(2));
                contact.setMidName(cursor.getString(3));
                contact.setAddress(cursor.getString(4));
                contact.setCity(cursor.getString(5));
                contact.setState(cursor.getString(6));
                contact.setZIP(cursor.getString(7));
                contact.setEmail(cursor.getString(8));
                contact.setPhoneNumber(cursor.getString(9));
                // Adding contact to list
                memberContactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        Log.d(TAG, "List has " + memberContactList.size() + " elements.");
        return memberContactList;
    }

    public static List<Contact> getAllLeaderContacts() {

        if (leaderContactList == null) {
            leaderContactList = new ArrayList<Contact>();
        }

        if (leaderContactList.size() != 0) {
            Log.d(TAG, "List has " + leaderContactList.size() + " elements.");
            return leaderContactList;
        }
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LEADER_CONTACTS;

        Log.d (TAG, "getAllLeaderContacts : " + selectQuery);

        if (!readDB) {
            int rowCnt = data.length;
            int colCnt = data[0].length;
            for (int row = 0; row < rowCnt; row++) {
                for (int col = 0; col < colCnt; col++) {
                    Contact contact = new Contact();
                    contact.setLastName(data[row][0]);
                    contact.setFirstName(data[row][1]);
                    contact.setMidName(data[row][2]);
                    contact.setAddress(data[row][3]);
                    contact.setCity(data[row][4]);
                    contact.setState(data[row][5]);
                    contact.setZIP(data[row][6]);
                    contact.setEmail(data[row][7]);
                    contact.setPhoneNumber(data[row][8]);
                    // Adding contact to list
                    leaderContactList.add(contact);
                }
            }
            return leaderContactList;
        }

        //SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = mDb.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                // contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setLastName(cursor.getString(1));
                contact.setFirstName(cursor.getString(2));
                //contact.setMidName(cursor.getString(3));
                contact.setMidName("");
                contact.setAddress(cursor.getString(3));
                contact.setCity(cursor.getString(4));
                contact.setState(cursor.getString(5));
                contact.setZIP(cursor.getString(6));
                contact.setEmail(cursor.getString(8));
                contact.setPhoneNumber(cursor.getString(7));
                // Adding contact to list
                leaderContactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        Log.d(TAG, "List has " + leaderContactList.size() + " elements.");
        return leaderContactList;
    }
}