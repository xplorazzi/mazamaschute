package com.xplorazzi.MazamasChute;

/**
 * Created by Pranabesh on 11/14/2015.
 */
public class Contact {
    //private variables
    int _id;
    String _first_name;
    String _last_name;
    String _mid_name;
    String _address;
    String _city;
    String _state;
    String _zip;
    String _phone_number;
    String _email;

    // Empty constructor
    public Contact(){

    }
    // constructor
    public Contact(int id, String first_name, String last_name, String mid_name,
                   String address, String city, String state, String zip,
                   String phone_number, String email){
        this._id = id;
        this._first_name = first_name;
        this._last_name = last_name;
        this._mid_name = mid_name;
        this._address = address;
        this._city = city;
        this._state = state;
        this._zip = zip;
        this._phone_number = phone_number;
        this._email = email;
    }

    public Contact(String first_name, String last_name, String mid_name,
                   String address, String state, String zip,
                   String phone_number, String email){

        this._first_name = first_name;
        this._last_name = last_name;
        this._mid_name = mid_name;
        this._address = address;
        this._state = state;
        this._zip = zip;
        this._phone_number = phone_number;
        this._email = email;
    }

    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getFirstName(){
        return this._first_name;
    }

    // setting name
    public void setFirstName(String name){
        this._first_name = name;
    }

    public String getLastName(){
        return this._last_name;
    }

    // setting name
    public void setLastName(String name){
        this._last_name = name;
    }

    public void setMidName(String middleName) {
        this._mid_name = middleName;
    }
    public String getMidName() {
        return this._mid_name;
    }
    // getting phone number
    public String getPhoneNumber(){
        return this._phone_number;
    }

    // setting phone number
    public void setPhoneNumber(String phone_number){
        this._phone_number = phone_number;
    }

    public  void setAddress(String address) {
        this._address = address;
    }
    public String getAddress() {
        return this._address;
    }

    public void setEmail(String email) {
        this._email = email;
    }
    public String getEmail() {
        return this._email;
    }

    public void setCity(String city) {
        this._city = city;
    }
    public String getCity() {
        return this._city;
    }

    public void setState (String state) {
        this._state = state;
    }
    public String getState() {
        return this._state;
    }
    public void setZIP(String ZIP) {
        this._zip = ZIP;
    }
    public String getZIP() {
        return this._zip;
    }
}
