package com.xplorazzi.MazamasChute;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ListIterator;


/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentSimpleLoginButton extends Fragment {

    private String TAG = "FragmentSimpleLoginButton";

    private TextView mTextDetails;
    private CallbackManager mCallbackManager;
    private AccessToken accessToken;
    private Profile profile;
    private AccessTokenTracker mTokenTracker;
    private ProfileTracker mProfileTracker;
    private String FirstName;
    private String LastName;
    private String Email;

    private static boolean isLoggedOut = true;
    private static boolean isAMember = false;

    public interface OnCompleteListener {
        void onComplete();
    }

    private OnCompleteListener mListener;

    public boolean profileIsLoggedIn() {
        if (profile != null) return true;
        else return false;
    }

    public boolean profileIsAMember() {
        profile = Profile.getCurrentProfile();


        int rowCnt = DataBaseHelper.data.length;
        int colCnt = DataBaseHelper.data[0].length;

        if (profile == null) {
            return false;
        } else {
            FirstName = profile.getFirstName();
            LastName = profile.getLastName();
        }
        Log.d(TAG, "First Last Name " + FirstName + " " + LastName);

        ListIterator<Contact> litr = null;
        Contact next;
        litr = DataBaseHelper.getAllMemberContacts().listIterator();

        //if (isLoggedOut == false) return isAMember;

        //Log.d(TAG, "Print the list");
        while (litr.hasNext()) {
            next = litr.next();
            //Log.d(TAG, next.getFirstName() + next.getLastName());
            if (next.getFirstName().equals(FirstName) && next.getLastName().equals(LastName)) {
                //TBD: Add handling for 2 Tim Scott Entries - have to go through complete database to see if there are multiple name matches.
                if (next.getEmail().isEmpty()) {
                    //Log.d(TAG, "No Email provided to Mazamas. Please update FB Email in Mazama DB");
                    showToast("No Email provided to Mazamas. Please update FB Email in Mazama DB");
                    break;
                } else if (Email != null && next.getEmail().equals(Email)) {
                    Log.d(TAG, "Checking Matched Email:");
                    isAMember = true;
                    break;
                } else if (Email == null) {
                    Log.d(TAG, "Still logging in Email: null");
                    isAMember = false;
                    break;
                } else {
                    Log.d(TAG, "DB Email: " + Email);
                    //Log.d(TAG, "Email provided to Mazamas DB does not match FB Email.");
                    showToast("Email provided to Mazamas DB does not match FB Email.");
                    break;
                }
            }
        }
        //Log.d(TAG, "Done Printing");
        /*
        for (int row = 0; row < rowCnt; row++) {
            //Log.d(TAG, data[row][0]+ " " + data[row][1]);
            if (data[row][1].equals(FirstName) && data[row][0].equals(LastName)) {
                found = true;
                break;
            }
        }
        */
        return isAMember;
    }

    private void facebokGraphRequest() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {
                JSONObject json = response.getJSONObject();
                try {
                    if(json != null){
                        Email = json.getString("email");
                        Log.d(TAG, "Email: " + Email);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        //parameters.putString("fields", "id,name,link,email,picture");
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.d(TAG, "onSuccess");
            accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();

            facebokGraphRequest();

            if (profileIsAMember()) {
                Log.d(TAG, "Found " + profile.getName());
            }
            Log.d(TAG, "FB onSuccess Welcome Message");
            isLoggedOut = false;
            mTextDetails.setText(constructWelcomeMessage(profile));
        }

        @Override
        public void onCancel() {
            Log.d(TAG, "FB:onCancel");
        }

        @Override
        public void onError(FacebookException e) {

            Log.d(TAG, "FB:onError " + e);
        }
    };

    public FragmentSimpleLoginButton() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        accessToken = AccessToken.getCurrentAccessToken();
        profile = Profile.getCurrentProfile();

        Log.d(TAG, "Fragment:onCreate");
        //mListener.onComplete();
        Log.d(TAG, accessToken == null ? ">>>" + "Signed Out" : ">>>" + "Signed In");

        // If already logged in then get the Email.
        if (accessToken != null) facebokGraphRequest();

        mCallbackManager = CallbackManager.Factory.create();

        setupTokenTracker();

        setupProfileTracker();

        mTokenTracker.startTracking();
        mProfileTracker.startTracking();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "Fragment:onCreateView");
        return inflater.inflate(R.layout.fragment_simple_login_button, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d(TAG, "Fragment:onViewCreated Begin");
        setupTextDetails(view);
        setupLoginButton(view);

        Log.d(TAG, "Fragment:onViewCreated End");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        populateViewForOrientation(inflater, (ViewGroup) getView());
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "Fragment:onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        Log.d(TAG, "Fragment:onResume Welcome Message");
        mTextDetails.setText(constructWelcomeMessage(profile));
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "Fragment:onStop");
        mTokenTracker.stopTracking();
        mProfileTracker.stopTracking();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "FB onActivityResult");
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup viewGroup) {
        viewGroup.removeAllViewsInLayout();
        View subview = inflater.inflate(R.layout.fragment_simple_login_button, viewGroup);

        // Find your buttons in subview, set up onclicks, set up callbacks to your parent fragment or activity here.

        // You can create ViewHolder or separate method for that.
        // example of accessing views: TextView textViewExample = (TextView) view.findViewById(R.id.text_view_example);
        // textViewExample.setText("example");
    }

    private void setupTextDetails(View view) {
        mTextDetails = (TextView) view.findViewById(R.id.text_details);
    }

    private void setupTokenTracker() {
        mTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken != null) {
                    Log.d(TAG, "Access Token" + currentAccessToken);
                } else {
                    Log.d(TAG, FirstName + " " + LastName + " Logged Out");
                    mTextDetails.setText(FirstName + " " + LastName + " Logged Out");
                    isLoggedOut = true;
                    isAMember = false;
                }
                mListener.onComplete();
            }
        };
    }

    private void setupProfileTracker() {
        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (currentProfile != null) {
                    Log.d(TAG, "Profile Changed: " + currentProfile);
                }
                mTextDetails.setText(constructWelcomeMessage(currentProfile));
                //showToast(constructWelcomeMessage(currentProfile));
            }
        };
    }

    private void setupLoginButton(View view) {
        LoginButton mButtonLogin = (LoginButton) view.findViewById(R.id.login_button);
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Log.d(TAG, "Fragment:OnClick Listener called");
                                            }
                                        }
        );
        //setFragment needs support.v4 libary. Using onActivityResult in Parent Activity
        // to achieve same result.
        //mButtonLogin.setFragment();
        //mButtonLogin.setReadPermissions(Arrays.asList("public_profile, email, user_friends"));
        mButtonLogin.setReadPermissions("user_friends");
        mButtonLogin.registerCallback(mCallbackManager, mFacebookCallback);
    }

    private String constructWelcomeMessage(Profile profile) {
        StringBuffer stringBuffer = new StringBuffer();
        if (profile != null) {
            stringBuffer.append("Welcome " + profile.getName());
        }
        return stringBuffer.toString();
    }


    public void onAttach(Context activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");

        try {
            this.mListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}
