package com.xplorazzi.MazamasChute;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class MapAllActivity extends AppCompatActivity {
    private static String TAG = "MapAllActivity";

    //This arraylist will have data as pulled from server. This will keep cumulating.
    private static ArrayList<LatLng> productResults = new ArrayList<LatLng>();
    //Based on the search string, only filtered products will be moved here from productResults
    //private static ArrayList<String> filteredProductResults = new ArrayList<String >();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_all);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Log.d(TAG, "onCreate");

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }

    /*
    public Barcode.GeoPoint getLocationFromAddress(String strAddress){
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        Barcode.GeoPoint p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address==null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new Barcode.GeoPoint(1, (location.getLatitude() * 1E6), (location.getLongitude() * 1E6));
            return p1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    */

    class myAsyncTask extends AsyncTask<String, Void, String> {
        List<Contact> productList;
        String textSearch;
        ProgressDialog pd;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            productList = new ArrayList<Contact>();

            //pd= new ProgressDialog(getActivity());
            pd = new ProgressDialog(MapAllActivity.this);
            pd.setCancelable(false);
            //pd.setMessage("Mapping All Members...");
            pd.setMessage("Feature in development...");
            pd.getWindow().setGravity(Gravity.CENTER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... sText) {
            Log.d(TAG, "doInBackground");
            String returnResult = getAllMemberAddressList();
            this.textSearch = sText[0];
            return returnResult;
        }

        public LatLng getLocationFromAddress(Context context, String strAddress) {

            Geocoder coder = new Geocoder(context);
            List<Address> address;
            LatLng pos = null;
            try {
                address = coder.getFromLocationName(strAddress, 5);
                if (address == null) {
                    return null;
                }
                Address location = address.get(0);
                location.getLatitude();
                location.getLongitude();

                pos = new LatLng(location.getLatitude(), location.getLongitude());

            } catch (Exception ex) {

                ex.printStackTrace();
            }
            return pos;
        }

        public String getAllMemberAddressList() {
            String tempProduct;
            String matchFound = "N";
            //productResults is an arraylist with all product details for the search criteria
            //productResults.clear();
            Log.d(TAG, "getAllMemberAddressList");
            try {
                productList = DataBaseHelper.getAllMemberContacts();
                //parse date for dateList
                for (int i = 0; i < productList.size(); i++) {
                    Contact obj = productList.get(i);
                    tempProduct = obj.getAddress() + ", " +
                            obj.getCity() + ", " +
                            obj.getState() + " " +
                            obj.getZIP();
                    productResults.add(getLocationFromAddress(MapAllActivity.this, tempProduct));
                }
                return ("OK");
            } catch (Exception e) {
                e.printStackTrace();
                return ("Exception Caught");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase("Exception Caught")) {
                Toast.makeText(MapAllActivity.this, "Unable to connect to server,please try later", Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                //calling this method to filter the search results from productResults and move them to
                //filteredProductResults
                //filterProductArray(textSearch);
                //searchResults.setAdapter(new SearchResultsAdapter(MapAllActivity.this, MapAllActivity.this, productResults));
                pd.dismiss();
            }
        }
    }

    /*
    private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
        private List<OverlayItem> items=new ArrayList<OverlayItem>();

        public SitesOverlay(Drawable marker) {
            super(marker);

            boundCenterBottom(marker);

            items.add(new OverlayItem(getPoint(40.748963847316034,
                    -73.96807193756104),
                    "UN", "United Nations"));
            items.add(new OverlayItem(getPoint(40.76866299974387,
                    -73.98268461227417),
                    "Lincoln Center",
                    "Home of Jazz at Lincoln Center"));
            items.add(new OverlayItem(getPoint(40.765136435316755,
                    -73.97989511489868),
                    "Carnegie Hall",
                    "Where you go with practice, practice, practice"));
            items.add(new OverlayItem(getPoint(40.70686417491799,
                    -74.01572942733765),
                    "The Downtown Club",
                    "Original home of the Heisman Trophy"));

            populate();
        }

        @Override
        protected OverlayItem createItem(int i) {
            return(items.get(i));
        }

        @Override
        protected boolean onTap(int i) {
            Toast.makeText(NooYawk.this,
                    items.get(i).getSnippet(),
                    Toast.LENGTH_SHORT).show();

            return(true);
        }

        @Override
        public int size() {
            return(items.size());
        }
    }
*/
}
