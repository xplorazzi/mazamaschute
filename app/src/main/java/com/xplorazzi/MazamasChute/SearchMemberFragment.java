package com.xplorazzi.MazamasChute;

/**
 * Created by Pranabesh on 11/16/2015.
 */

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SearchMemberFragment extends Fragment implements SearchResultsAdapter.OnPhoneCallListener,
                                                      SearchResultsAdapter.OnWriteEmailListener,
                                                      SearchResultsAdapter.OnMapAddressListener {

    private static String TAG = "SearchMemberFragment";
    private View myFragmentView;
    private SearchView mSearch;

    private Typeface type;
    private ListView searchResults;
    private String found = "N";
    private Context mActivity;

    //private TextView txtSpeechInput;
    private ImageButton btnSpeak;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    public interface OnSearchListener {
        boolean onSearch();
    }

    private OnSearchListener mSearchEntry;

    //This arraylist will have data as pulled from server. This will keep cumulating.
    private static ArrayList<Contact> productResults = new ArrayList<Contact>();
    //Based on the search string, only filtered products will be moved here from productResults
    private static ArrayList<Contact> filteredProductResults = new ArrayList<Contact>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");
        mActivity = activity;
        try {
            this.mSearchEntry = (OnSearchListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnSearchListener");
        }
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "Configuration Changed");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        populateViewForOrientation(inflater, (ViewGroup) getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //get the context of the HomeScreen Activity
        //define a typeface for formatting text fields and listview.
        Log.d(TAG, "onCreateView");
        if (mActivity == null) {
            Log.e(TAG, "mActivity is NULL");
        }
        if (mActivity.getAssets() == null) {
            Log.e(TAG, "mActivity.getAssets() is NULL");
        }
        type = Typeface.createFromAsset(mActivity.getAssets(), "fonts/book.TTF");
        myFragmentView = inflater.inflate(R.layout.fragment_search, container, false);

        Log.d(TAG, "Find the SearchView");
        mSearch = (SearchView) myFragmentView.findViewById(R.id.searchView);
        //mSearch.setFocusable(true);
        mSearch.setIconified(false);
        //mSearch.requestFocusFromTouch();

        setQueryHint("Member Search...");

        searchResults = (ListView) myFragmentView.findViewById(R.id.listview_search);
        btnSpeak = (ImageButton) myFragmentView.findViewById(R.id.imageButton);
        //txtSpeechInput = (TextView) myFragmentView.findViewById(R.id.txtSpeechInput);

        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
        //this part of the code is to handle the situation when user enters any search criteria, how should the
        //application behave?

        mSearch.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                //Toast.makeText(activity, String.valueOf(hasFocus),Toast.LENGTH_SHORT).show();
                Log.d(TAG, " onFocusChange");
                if (mSearchEntry.onSearch() == false) {
                    setQueryHint("Members Directory...");
                    clearSearchLists();
                    return;
                }
            }
        });

        mSearch.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub
                Log.d(TAG, "onQueryTextSubmit");
                if (mSearchEntry.onSearch() == false) {
                    setQueryHint("Members Directory...");
                    clearSearchLists();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange");
                if (mSearchEntry.onSearch() == false) {
                    setQueryHint("Members Directory...");
                    clearSearchLists();
                    return false;
                }
                if (newText.length() > 2) {
                    searchResults.setVisibility(myFragmentView.VISIBLE);
                    myAsyncTask m = (myAsyncTask) new myAsyncTask().execute(newText);
                } else {
                    searchResults.setVisibility(myFragmentView.INVISIBLE);
                }
                return false;
            }
        });
        return myFragmentView;
    }

    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult");
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text;
                    Log.d(TAG, "Voice Search: ");
                    text = result.get(0);
                    for (int i=0; i<result.size(); i++) {
                       Log.d(TAG, result.get(i));
                    }
                    mSearch.setQuery(text, true);
                    if (mSearchEntry.onSearch() == false) {
                        setQueryHint("Members Directory...");
                        clearSearchLists();
                        return;
                    }
                    setQueryHint("Voice Search...");
                    if (text.length() > 2) {
                        searchResults.setVisibility(myFragmentView.VISIBLE);
                        myAsyncTask m = (myAsyncTask) new myAsyncTask().execute(text);
                    } else {
                        searchResults.setVisibility(myFragmentView.INVISIBLE);
                    }
                }
                break;
            }
        }
        return;
    }

    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(mActivity.getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup viewGroup) {
        viewGroup.removeAllViewsInLayout();
        View subview = inflater.inflate(R.layout.fragment_search, viewGroup);

        // Find your buttons in subview, set up onclicks, set up callbacks to your parent fragment or activity here.

        // You can create ViewHolder or separate method for that.
        // example of accessing views: TextView textViewExample = (TextView) view.findViewById(R.id.text_view_example);
        // textViewExample.setText("example");
    }

    public void setQueryHint(String text) {
        Log.d(TAG, "QueryHint: " + text);
        if (mSearch == null) {
            Log.d(TAG, "SearchView is Null");
        } else {
            mSearch.setQueryHint(null);
            mSearch.setQueryHint(text);
        }
    }

    public void setQuery(String text) {
        Log.d(TAG, "Query: " + text);
        if (mSearch == null) {
            Log.d(TAG, "SearchView is Null");
        } else {
            mSearch.setQuery(text, false);
            mSearch.setIconified(true);
        }
    }

    public static void clearSearchLists() {
        productResults.clear();
        filteredProductResults.clear();
    }
    //this filters products from productResults and copies to filteredProductResults based on search text

    public void filterProductArray(String newText) {
        String pFirstName, pLastName;

        filteredProductResults.clear();
        for (int i = 0; i < productResults.size(); i++) {
            pFirstName = productResults.get(i).getFirstName().toLowerCase();
            pLastName = productResults.get(i).getLastName().toLowerCase();
            if (pFirstName.contains(newText.toLowerCase()) ||
                    pLastName.contains(newText.toLowerCase())) {
                filteredProductResults.add(productResults.get(i));
            }
        }
    }

    class myAsyncTask extends AsyncTask<String, Void, String> {
        List<Contact> productList;
        String textSearch;
        ProgressDialog pd;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            productList = new ArrayList<Contact>();

            //pd= new ProgressDialog(getActivity());
            pd = new ProgressDialog(mActivity);
            pd.setCancelable(false);
            pd.setMessage("Searching...");
            pd.getWindow().setGravity(Gravity.CENTER);
            pd.show();
        }

        @Override
        protected String doInBackground(String... sText) {
            Log.d(TAG, "doInBackground");
            String returnResult = getAllMemberContactList();
            this.textSearch = sText[0];
            return returnResult;
        }

        public String getAllMemberContactList() {
            Contact tempProduct;
            String matchFound = "N";
            //productResults is an arraylist with all product details for the search criteria
            //productResults.clear();
            Log.d(TAG, "getAllMemberContactList");
            try {
                productList = DataBaseHelper.getAllMemberContacts();

                //parse date for dateList
                for (int i = 0; i < productList.size(); i++) {
                    tempProduct = new Contact();
                    Contact obj = productList.get(i);

                    tempProduct.setFirstName(obj.getFirstName());
                    tempProduct.setLastName(obj.getLastName());
                    tempProduct.setMidName(obj.getMidName());
                    tempProduct.setAddress(obj.getAddress());
                    tempProduct.setCity(obj.getCity());
                    tempProduct.setState(obj.getState());
                    tempProduct.setZIP(obj.getZIP());
                    tempProduct.setPhoneNumber(obj.getPhoneNumber());
                    tempProduct.setEmail(obj.getEmail());

                    //check if this product is already there in productResults, if yes, then don't add it again.
                    matchFound = "N";

                    for (int j = 0; j < productResults.size(); j++) {
                        if (productResults.get(j).getFirstName().equals(tempProduct.getFirstName()) &&
                                productResults.get(j).getLastName().equals(tempProduct.getLastName()) &&
                                productResults.get(j).getMidName().equals(tempProduct.getMidName())) {
                            matchFound = "Y";
                        }
                    }

                    if (!matchFound.equals("N")) {
                        productResults.add(tempProduct);
                    }

                }
                return ("OK");
            } catch (Exception e) {
                e.printStackTrace();
                return ("Exception Caught");
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase("Exception Caught")) {
                Toast.makeText(getActivity(), "Unable to connect to server,please try later", Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                //calling this method to filter the search results from productResults and move them to
                //filteredProductResults
                filterProductArray(textSearch);
                searchResults.setAdapter(new SearchResultsAdapter(getActivity(), SearchMemberFragment.this, filteredProductResults));
                pd.dismiss();
            }
        }
    }

    public void onPhoneCall(String text) {
        Log.d(TAG, "Making Phone call");
        String number = "tel:" + text.trim();
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
        SearchMemberFragment.this.startActivity(callIntent);
    }

    public void onWriteEmail(String text) {
        Log.d(TAG, "Writing Email");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{text});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT, "body of email");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mActivity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onMapAddress (String text) {
        Log.d(TAG, "Mapping Address");
        //text = text.replace(',','+');
        //text = text.replace(' ','+');
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=0,0 (%s)", text);
        //String uri = String.format(Locale.ENGLISH, "geo:0,0?q=", text);
        Log.d(TAG, "URI:" + uri);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
        //    Log.e(TAG, ex.toString());

         try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(mActivity, "Please install a maps application", Toast.LENGTH_LONG).show();
            }

        }
    }
}

